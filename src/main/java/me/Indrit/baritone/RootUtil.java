package me.Indrit.baritone;

import me.deftware.client.framework.main.Bootstrap;
import me.deftware.client.framework.utils.ChatColor;
import me.deftware.client.framework.wrappers.IChat;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class RootUtil {
    public boolean checkNetConnection() {
        try (Socket socket = new Socket()) {
            int port = 80;
            InetSocketAddress socketAddress = new InetSocketAddress("google.com", port);
            socket.connect(socketAddress, 200);

            return true;
        } catch (IOException e) {
            message(ChatColor.GRAY + "Internet connection is not available! Aborting all internet tasks...");
            LogManager.getLogger("Baritone/Fabritone Installer").error("Failed to establish internet connection");
            Bootstrap.callMethod("EMC-Marketplace", "setStatus(Could not install Baritone, see latest.log)", "Baritone Installer", null);
            return false;
        }
    }
    private void message(String msg) {
        IChat.sendClientMessage(msg, ChatColor.DARK_PURPLE + "[" + ChatColor.LIGHT_PURPLE + "Baritone" + ChatColor.DARK_PURPLE + "]");
    }
}
